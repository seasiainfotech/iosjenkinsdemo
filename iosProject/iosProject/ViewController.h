//
//  ViewController.h
//  iosProject
//
//  Created by Paramvir Singh on 3/23/17.
//  Copyright © 2017 Cerebrum. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

-(int)sumOfNumber:(int)firstNum secondNum:(int)secondNum;

@end

