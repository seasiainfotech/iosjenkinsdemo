//
//  AppDelegate.h
//  iosProject
//
//  Created by Paramvir Singh on 3/23/17.
//  Copyright © 2017 Cerebrum. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

