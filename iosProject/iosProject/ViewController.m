//
//  ViewController.m
//  iosProject
//
//  Created by Paramvir Singh on 3/23/17.
//  Copyright © 2017 Cerebrum. All rights reserved.
//

#import "ViewController.h"



@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view, typically from a nib.
}

-(int)sumOfNumber:(int)firstNum secondNum:(int)secondNum{
    return firstNum+secondNum;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
