//
//  iosProjectTests.m
//  iosProjectTests
//
//  Created by Paramvir Singh on 3/23/17.
//  Copyright © 2017 Cerebrum. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "ViewController.h";

@interface iosProjectTests : XCTestCase

@end

@implementation iosProjectTests


- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    ViewController *con = [[ViewController alloc] init];
    
    int sum =   [con sumOfNumber:2 secondNum:5];
    
    XCTAssertEqual(7, sum);
    
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
